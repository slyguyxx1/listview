﻿using System;
using Xamarin.Forms;

namespace leagueItemView.Models
{
    public class WebsiteItem
    {
        public string websiteName
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }
    }
}
