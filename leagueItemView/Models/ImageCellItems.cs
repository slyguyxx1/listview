﻿using System;
using Xamarin.Forms;

namespace leagueItemView.Models
{
    public class ImageCellItems
    {
        public string ImageText
        {
            get;
            set;
        }

        public ImageSource IconSource
        {
            get;
            set;
        }

        public string Details
        {
            get;
            set;
        }

        public string websiteName
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }


    }
}
