﻿using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using leagueItemView.Models;
using System;

namespace leagueItemView
{
    public partial class leagueItemViewPage : ContentPage
    {
        public leagueItemViewPage()
        {
            InitializeComponent();

            PopulateColorList();
        }

        private void PopulateColorList()
        {
            var imageForListView = new ObservableCollection<ImageCellItems>()
            {
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("infinityEdge.jpg"),
                    ImageText = "IFINITY EDGE",
                    Details= "gives increased critical hit damage.",
                    url = "http://leagueoflegends.wikia.com/wiki/Infinity_Edge",
                },
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("pd.jpeg"),
                    ImageText = "PHANTOM DANCER",
                    Details="gives attack speed and dueling power.",
                    url = "http://leagueoflegends.wikia.com/wiki/Phantom_Dancer",

                        
                },
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("bt.png"),
                    ImageText = "BLOOD THIRSTER",
                    Details= "gives lifesteal on attacks.",
                    url = "http://leagueoflegends.wikia.com/wiki/The_Bloodthirster"

                },
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("trinity.jpg"),
                    ImageText = "TRINITY FORCE",
                    Details= "gives a myriad of stats.",
                    url = "http://leagueoflegends.wikia.com/wiki/Trinity_Force",
                },
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("shiv.jpeg"),
                    ImageText = "STATIC SHIVE",
                    Details= "gives an aoe on attacks.",
                    url = "http://leagueoflegends.wikia.com/wiki/Statikk_Shiv",
                },
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("er.jpeg"),
                    ImageText = "ESSENCE REAVER",
                    Details= " gives critical strike chance and CDR.",
                    url = "http://leagueoflegends.wikia.com/wiki/Essence_Reaver",
                },
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("mr.png"),
                    ImageText = "MORTAL REMINDER",
                    Details= "reduces healing on targets and penetrates armor.",
                    url = "http://leagueoflegends.wikia.com/wiki/Mortal_Reminder",
                },
                new ImageCellItems()
                {
                    IconSource = ImageSource.FromFile("bork.png"),
                    ImageText = "BLADE OF THE RUINED KING",
                    Details= "gives % health damage.",
                    url = "http://leagueoflegends.wikia.com/wiki/Blade_of_the_Ruined_King",
                },

            };

            ImageCellsListView.ItemsSource = imageForListView;

        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            ImageCellItems itemTapped = (ImageCellItems)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }
    }
}
